// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBUtOmYDfNgoOu-Uisk3Ms5g9gdBC2lu10",
    authDomain: "hello-c321a.firebaseapp.com",
    databaseURL: "https://hello-c321a.firebaseio.com",
    projectId: "hello-c321a",
    storageBucket: "hello-c321a.appspot.com",
    messagingSenderId: "1095455424229",
    appId: "1:1095455424229:web:f71e0bd0cac9e850365e0c"
  }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
